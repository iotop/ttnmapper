/************************** Configuration ***********************************/
#include <TinyLoRa.h>
#include <SPI.h>

// Visit your thethingsnetwork.org device console if you need your session keys.

// Network Session Key (MSB)
uint8_t NwkSkey[16] = { FILL_ME_IN };

// Application Session Key (MSB)
uint8_t AppSkey[16] = { FILL_ME_IN };

// Device Address (MSB)
uint8_t DevAddr[4] = { FILL_ME_IN };

/************************** DO NOT EDIT BELOW THIS LINE  ***********************************/
// Data Packet to Send to TTN
unsigned char loraData[1] = {"T"};

// How many times data transfer should occur, in seconds
const unsigned int sendInterval = 5;

// Light will blink for 200ms 
int blinktime = 200;

// Pinout for Adafruit Feather 32u4 LoRa
TinyLoRa lora = TinyLoRa(7, 8, 4);

void setup()
{
  delay(5000);
  Serial.begin(115200);
  
  // Initialize pin LED_BUILTIN as an output
  pinMode(LED_BUILTIN, OUTPUT);
  
  // Initialize LoRa
  Serial.print("Starting LoRa...");
  // define multi-channel sending
  lora.setChannel(MULTI);
  // set datarate
  lora.setDatarate(SF7BW125);
  if(!lora.begin())
  {
    Serial.println("Failed");
    Serial.println("Check your radio");
    while(true);
  }
  Serial.println("OK");
}

void loop()
{
  Serial.println("Sending LoRa Data...");
  lora.sendData(loraData, sizeof(loraData), lora.frameCounter);
  Serial.print("Frame Counter: ");Serial.println(lora.frameCounter);
  lora.frameCounter++;

  // blink LED to indicate packet sent
  digitalWrite(LED_BUILTIN, HIGH);
  delay(blinktime);
  digitalWrite(LED_BUILTIN, LOW);
  
  Serial.println("delaying...");
  delay((sendInterval * 1000) - blinktime);
}
