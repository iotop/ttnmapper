# TTN Mapper

This repo is dedicated to our contribution to [TTN Mapper](https://ttnmapper.org/). 

## Contributing Guidelines

 - If using a vehicle, stay below 50km/h if possible to reduce doppler effect.
 - Do not use spreading factors other than the default (SF7BW125).
 - Adhere to local broadcasting laws, do not exceed 1% duty cycle.
 - Attempt to map previously uncharted areas, reduce repeated routes.
 - Ensure our newly deployed gateways have an accurate location set in the TTN console before mapping.
 - If you're driving with a node make sure it's positioned high enough to not be obscured by body panels. Your dashboard should be fine. 
 - Drive safe!
 
## Programming a node

This repo contains a few example sketches for the nodes we currently use. You'll need to populate the sketches with keys issued by TTN. 

1. Download the appropriate sketch based on which device you're using. 
2. Open the sketch with the Arduino IDE
3. Make sure the current port and board are selected within the "Tool" menu. (TTGO LoRa32 == Heltec WiFi LoRa 32)
4. [Log in](https://account.thethingsnetwork.org/users/login) to the OPIoT TTN account with the credentials saved in our sensitive repo.
5. Navigate to the [op_ttnmapper application](https://console.thethings.meshed.com.au/applications/op_ttnmapper). 
6. Register a new device within the application and give it a useful device ID.
7. Let TTN automatically generate the device EUI. 
8. Once you've created your new device, navigate to the settings panel for the device.
9. Change the activation method to 'ABP'.
10. Uncheck the "Frame Counter Checks" box, this will save headaches later. 
11. Copy the Device Address, Network Session Key and App Session Key from the TTN console into your Arduino sketch. 
12. Upload the sketch to your board, check that TTN is receiving data. 

If the status shows as "never seen" make sure your keys are correct and the device is blinking every 5 seconds.

## Phone Setup

### Android

1. Download and install the [TTN Mapper App](https://play.google.com/store/apps/details?id=org.ttnmapper.phonesurveyor&hl=en).
2. Open the app, navigate to the settings menu.
3. Make sure "LoRdrive mode" and "Upload data" are both enabled.
4. Under the linked device section, fill in "Application ID" with `op_ttnmapper`.
5. For the application key, you'll need to copy and paste the access key from [here](https://console.thethings.meshed.com.au/applications/op_ttnmapper). You may need to log in to the OPIoT account from your phones browser. 
6. Set the "Device ID" to the name you gave to your node in TTN. Leaving this value as '+' will subscribe to all device messages. 
7. Set the "MQTT URL" to `tcp://thethings.meshed.com.au:1883`.
 
### iPhone
1. Get an android phone. Follow the above steps. 

## Mapping 

### Requirements:

 - LoRa Node
 - 5V/USB Power
 - Smartphone (Android or iPhone)
 - Accurate GPS 
 - Mobile data
 
### Getting Started
 1. Plug in your LoRa node to a stable power supply, a USB phone changer should suffice as these devices don't use very much power. 
 2. Open the "TTN Mapper" app on your phone. 
 3. Navigate to the "Map" within the app.
 4. Press the play button, your device will attempt to get a GPS lock and subscribe to MQTT. 
 5. In the bottom left of your screen you should see the status of both MQTT and GPS. 
 6. Assuming everything is set up correctly you should see "GPS Location valid" and "MQTT Connected"
 7. Start moving! You should begin seeing live location data immediately. 

### App Troubleshooting 

#### GPS
##### GPS Finding fix
GPS may be disabled.
##### GPS location not accurate enough
Ensure your phone has decent GPS visibility, leaving your phone on your dash or in a windscreen cradle is ideal.

#### MQTT
##### MQTT failed to connect - client exception
Make sure your phone has stable internet access. Disable WiFi when driving.
##### MQTT failed to connect - not authorised
Either the Application ID or Application Key are incorrect. The MQTT URL may also be wrong.
##### MQTT failed to connect - unexpected error
Make sure the MQTT URL is correct with the correct port information. (Default is 1883)
